<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'joehockey');

/** MySQL database username */
define('DB_USER', 'joehockey');

/** MySQL database password */
define('DB_PASSWORD', 'joehockey');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MA<?$oT9CD!5B3FmDFGE ^9-$C:Y)q.]JFYY2k*L]jesC89GxNTv!C)Cg%b;1YQS');
define('SECURE_AUTH_KEY',  'odb;#KxPtM[mWJ,uIZ~|&57rpAIA^tEIBqEf8^^1C0E(_Z9$fok9m=0u6[YozFY8');
define('LOGGED_IN_KEY',    'RK8vnMeZ=[J*;Ly;Il$LkeEu%+Ztx^:/x1:(gq:D(!|R&V0.a;</sGNEKz+v+acn');
define('NONCE_KEY',        'Q8}cPT`]^OGcy86%|qYl|/=)g2]cf[77x>C^-VM]eW=~~;EZtEY0MRt~oDo=iE}B');
define('AUTH_SALT',        '<c{HX})q9)QRxRX7}>:y3;vc<(E*R<Z}.9.RC|72oQ/4BW=Q_8s~W1[aXE2C)k%v');
define('SECURE_AUTH_SALT', 'SG-kBZ(>XQ2![tU+$4gkR{L*pn}k35OEw]J-;TUG]`L~R@jnl~}|%<H;%^P6IgNv');
define('LOGGED_IN_SALT',   'g?m]`<vDLnti`CP)_wQ*|>dQlb^kT`:m9Cp[}CVQukTd]%?3%bH[=ZiDct!SC_^A');
define('NONCE_SALT',       'w@W?g0NH$IPcBimCfdOs5%q6G# +ofj]|Pj(6V*W$Sbn,Hdxec(4ZD(6c41(9bnx');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
