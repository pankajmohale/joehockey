jQuery(document).ready(function($){

	// Initialize slick slider
	$( '.wpostahs-slider-nav' ).each(function( index ) {

		var sconf       	= {};
		var slider_id   	= $(this).attr('id');
		var slider_nav_id 	= $(this).attr('data-slider-nav-for');
		var slider_conf 	= $.parseJSON( $(this).parent('.wpostahs-slider-inner-wrp').parent('.wpostahs-slider-wrp').find('.wpostahs-slider-conf').text() );
		
		// For Navigation
		if( typeof(slider_nav_id) != 'undefined' && slider_nav_id != '' ) {
			nav_id = '.'+slider_nav_id;
		}

		if( typeof(slider_id) != 'undefined' && slider_id != '' ) {

			jQuery('.'+slider_nav_id).slick({
				dots			: (slider_conf.dots) 		== "true" ? true : false,
				infinite		: true,
				arrows			: false,
				speed 			: parseInt(slider_conf.speed),
				autoplay 		: (slider_conf.autoplay) 	== "true" ? true : false,
				fade 			: (slider_conf.fade) 		== "true" ? true : false,
				autoplaySpeed 	: parseInt(slider_conf.autoplayInterval),
				asNavFor 		: '#'+slider_id,
				slidesToShow 	: 1,
				slidesToScroll 	: 1,
				adaptiveHeight	: true
			});
		}

		// For Navigation
		if( typeof(slider_nav_id) != 'undefined' ) {
			
			jQuery('#'+slider_id).slick({
				slidesToShow 	: 5,
				slidesToScroll 	: 1,
				asNavFor 		: nav_id,
				arrows			: (slider_conf.arrows) 		== "true" ? true : false,
				dots 			: false,
				speed 			: parseInt(slider_conf.speed),
				centerMode 		: true,
				focusOnSelect 	: true,
				centerPadding 	: '10px',
				responsive 		: [
					{
						breakpoint: 768,
						settings: {
							arrows: true,
							centerMode: true,
							centerPadding: '10px',
							slidesToShow: 3
						}
					},
					{
						breakpoint: 480,
						settings: {
							arrows: true,
							centerMode: true,
							centerPadding: '10px',
							slidesToShow: 1
						}
					}
				]
			});
		}
	});
});