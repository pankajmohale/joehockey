<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package _s
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function _s_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', '_s_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function _s_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', '_s_pingback_header' );


add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    // add_image_size( 'category-thumb', 300 ); // 300 pixels wide (and unlimited height)
    add_image_size( 'scroller-gallery', 480, 315, true ); // (cropped)
    add_image_size( 'home-slider', 400, 265, true ); // (cropped)
    add_image_size( 'gallery-slider', 730, 410, true ); // (cropped)
    // add_image_size( 'gallery-slider-thumb', 250, 150, true ); // (cropped)
}


function get_breadcrumb() {
    echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        the_category(' &bull; ');
            if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
    } elseif (is_page()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo the_title();
    } elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
}

function custom_s_posted_on() {
    global $redux_demo;
    $categories_list = get_the_category_list( esc_html__( ', ', '_s' ) );
    ?>
    <div class="row">
        <div class="col-xs-6 col-sm-6">
            <?php echo '<span class="posted-on">' . get_the_date( 'M j, Y' ) . '</span><span class="byline"> ' . $categories_list . '</span>'; // WPCS: XSS OK. ?>
        </div>
        <div class="col-xs-6 col-sm-6">
            <div class="social-links text-right">
                <?php if ($redux_demo['facebook-links']) { ?>
                    <span class="facebook-links">
                        <a href="<?php echo $redux_demo['facebook-links']; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </span>
                <?php } ?>
                <?php if ($redux_demo['twitter-links']) { ?>
                    <span class="twitter-links">
                        <a href="<?php echo $redux_demo['twitter-links']; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </span>
                <?php } ?>
            </div>
        </div>
    </div>   
<?php 
}


/**
 * Add a custom sidebar.
 */
function wpdocs_joehockey_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Recent Tweets Sidebar', 'joehockey' ),
        'id'            => 'recent_tweets',
        'description'   => __( 'Widgets in this area will be shown on all tweets on deplomat section.', 'joehockey' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'wpdocs_joehockey_widgets_init' );
