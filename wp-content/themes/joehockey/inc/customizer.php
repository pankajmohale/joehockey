<?php
/**
 * _s Theme Customizer
 *
 * @package _s
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function _s_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', '_s_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function _s_customize_preview_js() {
	wp_enqueue_script( '_s_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', '_s_customize_preview_js' );

add_action( 'wp_enqueue_scripts', 'joyhockey_enqueue_script' );
function joyhockey_enqueue_script() {
	// Enque Js
	wp_deregister_script('jquery');
	wp_register_script('jquery', '/wp-includes/js/jquery/jquery.js', false, false, true);
	wp_enqueue_script('jquery');

    wp_register_script( 'isInViewport_min', get_template_directory_uri() . '/js/isInViewport.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'isInViewport_min' );

    wp_register_script( 'custom_js', get_stylesheet_directory_uri() . '/js/custom_js.js', array(), '1.0.0', true);
    wp_enqueue_script( 'custom_js' );

    wp_register_script( 'bootstrap_min', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'bootstrap_min' );
    

    wp_register_script( 'swiper_min_jquery', get_template_directory_uri() . '/js/swiper.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'swiper_min_jquery' );

    wp_register_script( 'swiper_jquery_min', get_template_directory_uri() . '/js/swiper.jquery.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'swiper_jquery_min' );


	

	// Enque CSS
    wp_register_style( 'bootstrap_min', get_template_directory_uri() .'/css/bootstrap.min.css', array(), '21');
	wp_enqueue_style( 'bootstrap_min' ); 

	wp_register_style( 'bootstrap_min_theme', get_template_directory_uri() .'/css/bootstrap-theme.min.css', array(), '21');
	wp_enqueue_style( 'bootstrap_min_theme' );

    wp_register_style( 'second-style', get_template_directory_uri() .'/css/second-style.css', array(), '21');
    wp_enqueue_style( 'second-style' ); 
    
	wp_register_style( 'bootstrap_font_awesome', get_template_directory_uri() .'/css/font-awesome.min.css', array(), '21');
	wp_enqueue_style( 'bootstrap_font_awesome' );

	wp_register_style( 'swiper_min_css', get_template_directory_uri() .'/css/swiper.min.css', array(), '22');
	wp_enqueue_style( 'swiper_min_css' );

    wp_register_style( 'animate_min', get_template_directory_uri() . '/css/animate.min.css', array(), '1');
    wp_enqueue_style( 'animate_min' );
}

/* 
* Register Nav menu
*/
register_nav_menus( array(
	'footer_menu' => esc_html__( 'Footer Menu', '_s' ),
) );

/*
* Add first and last menu
*/
function wpb_first_and_last_menu_class($items) {
    $items[1]->classes[] = 'first';
    $items[count($items)]->classes[] = 'last';
    return $items;
}
add_filter('wp_nav_menu_objects', 'wpb_first_and_last_menu_class');	

/* ================================================================================================================================ 
												Post Register taxonomy 
===================================================================================================================================*/
/* ************************ Post Type ********************** */
add_action( 'init', 'joehockey_post_type_register_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function joehockey_post_type_register_init() {
    $labels = array(
        'name'               => _x( 'Charities', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Charity', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Charities', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Charity', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Charity', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Charity', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Charity', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Charity', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Charity', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Charities', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Charities', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Charities:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No charities found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No charities found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'charity' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'charity', $args );
}
/* ================================================================================================================================ 
												Post Register taxonomy 
===================================================================================================================================*/
// hook into the init action and call create_custom_taxonomies when it fires
add_action( 'init', 'create_custom_taxonomies', 0 );

// create taxonomies diplomat for the post type "book"
function create_custom_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$gallery_type_labels = array(
		'name'              => _x( 'Gallery Types', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Gallery Type', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Gallery Types', 'textdomain' ),
		'all_items'         => __( 'All Gallery Types', 'textdomain' ),
		'parent_item'       => __( 'Parent Gallery Type', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Gallery Type:', 'textdomain' ),
		'edit_item'         => __( 'Edit Gallery Type', 'textdomain' ),
		'update_item'       => __( 'Update Gallery Type', 'textdomain' ),
		'add_new_item'      => __( 'Add New Gallery Type', 'textdomain' ),
		'new_item_name'     => __( 'New Gallery Type Name', 'textdomain' ),
		'menu_name'         => __( 'Gallery Type', 'textdomain' ),
	);

	$gallery_type_args = array(
		'hierarchical'      => true,
		'labels'            => $gallery_type_labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'gallery_type' ),
	);

	register_taxonomy( 'gallery_type', array( 'post' ), $gallery_type_args );
}

/* =============== Add new Taxonomy for Charity Type =============== */

// Add new taxonomy, make it hierarchical (like categories)
$labels = array(
    'name'              => _x( 'Charity Types', 'taxonomy general name', 'textdomain' ),
    'singular_name'     => _x( 'Charity Type', 'taxonomy singular name', 'textdomain' ),
    'search_items'      => __( 'Search Charity Types', 'textdomain' ),
    'all_items'         => __( 'All Charity Types', 'textdomain' ),
    'parent_item'       => __( 'Parent Charity Type', 'textdomain' ),
    'parent_item_colon' => __( 'Parent Charity Type:', 'textdomain' ),
    'edit_item'         => __( 'Edit Charity Type', 'textdomain' ),
    'update_item'       => __( 'Update Charity Type', 'textdomain' ),
    'add_new_item'      => __( 'Add New Charity Type', 'textdomain' ),
    'new_item_name'     => __( 'New Charity Type Name', 'textdomain' ),
    'menu_name'         => __( 'Charity Type', 'textdomain' ),
);

$args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'charity_types' ),
);

register_taxonomy( 'charity_types', array( 'charity' ), $args );