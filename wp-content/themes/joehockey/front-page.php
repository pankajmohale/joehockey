<?php
/**
 * The template for displaying Home Page
 */

get_header(); 
global $redux_demo;
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section id="diplomat" class="diplomat-section home-sections animated fadeOut custom-load-time">
				<!-- diplomat section -->
				<?php get_template_part( 'template-parts/diplomat', 'section' ); ?>
				<!-- diplomat section end -->
				
				<!-- diplomat gallery -->
				<?php get_template_part( 'template-parts/diplomat', 'gallery' ); ?>
				<!-- diplomat gallery end -->

				<!-- diplomat Speaches -->
				<?php get_template_part( 'template-parts/speeche', 'section' ); ?>
				<!-- diplomat Speaches End -->


			</section>
			<section id="parliament" class="parliament-section home-sections animated fadeOut custom-load-time">
				<!-- Parliament Speaches -->
				<?php get_template_part( 'template-parts/parliament', 'section' ); ?>
				<!-- Parliament Speaches End -->

				<!-- Parliment gallery -->
				<?php get_template_part( 'template-parts/parliament', 'gallery' ); ?>
				<!-- Parliment gallery end -->		

			</section>
			<section id="speechs" class="speechs-section home-sections animated fadeOut custom-load-time">

				<!-- Parliment gallery -->
				<?php get_template_part( 'template-parts/key', 'speeches' ); ?>
				<!-- Parliment gallery end -->
			
			</section>
			<!-- Government -->
			<section id="government" class="government-section home-sections animated fadeOut custom-load-time">
				<div class="home-section-content sections-padding">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="section-title-wrapper">
									<h2 class="section-title title-navy-blue text-center"><?php echo $redux_demo['home-section-third-title']; ?></h2>
								</div>
							</div>
						</div>
						<div class="timeline_wrapper">
							<?php echo do_shortcode('[cool-timeline]'); ?>
						</div>
					</div>
				</div>
			</section>
			<!-- Government End -->
			<section id="charity" class="charity-section home-sections animated fadeOut custom-load-time">
				<div class="home-section-content bg-sky-blue sections-padding">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="section-title-wrapper">
									<h2 class="section-title title-white text-center"><?php echo $redux_demo['home-section-fourth-title']; ?></h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="charity-wrapper">
								<!-- Charity section -->
								<?php get_template_part( 'template-parts/charity', 'section' ); ?>
								<!-- Charity Section end -->
							</div>
						</div>
					</div><!-- Container -->
				</div>
			</section>
			<section id="family" class="family-section home-sections animated fadeOut custom-load-time">
				<div class="home-section-content sections-padding">
					<?php get_template_part( 'template-parts/family', 'section' ); ?>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
