/*
* custom_js
*/

jQuery(document).ready(function($){
	$(".scroll_down").click(function() {
	    $('html,body').animate({
	        scrollTop: $("#diplomat").offset().top},
	        1000);
	});
	jQuery('header .home-header').height(jQuery(window).height() );

	if( jQuery(this).width() > 320 && jQuery(this).width() < 480 ) {
		// code
		jQuery('footer .footer-banner').height(jQuery(window).height() * 2.5);
	
	} else if( jQuery(this).width() > 481 && jQuery(this).width() < 767 ) {
		// code
		jQuery('footer .footer-banner').height(jQuery(window).height() * 2.4);
	
	} else if( jQuery(this).width() > 768 && jQuery(this).width() < 991 ) {
		// code
		jQuery('footer .footer-banner').height(jQuery(window).height() * 1.8);
	
	} else if( jQuery(this).width() > 992 && jQuery(this).width() < 1199 ) {
		// code
		jQuery('footer .footer-banner').height(jQuery(window).height() * 1.5);
	} else {
		jQuery('footer .footer-banner').height(jQuery(window).height() + 350);
	}

	jQuery('.header-menu .modal-content').height(jQuery(window).height());

	jQuery('body').on('shown.bs.modal', function () {
		if(jQuery(window).width() < 768 ) {
			jQuery('#diplomatgallery .modal-content').height(jQuery(window).height());
			jQuery('#parliamentgallery .modal-content').height(jQuery(window).height());
		}

		if(jQuery(window).width() >= 768 && jQuery(window).width() <= 1024 ) {
			jQuery('#diplomatgallery .modal-content').height(jQuery(window).height());
			jQuery('#parliamentgallery .modal-content').height(jQuery(window).height());
		}

		if(jQuery(window).width() >= 768 ) {
			jQuery('#diplomatgallery .modal-content').height(jQuery(window).height());
			jQuery('#parliamentgallery .modal-content').height(jQuery(window).height());
		}	
	});

	jQuery('.diplomat-gallery-btn .photo-gallery-btn').click(function(){
		jQuery('body').on('shown.bs.modal', function () {
			jQuery('#diplomatgallery .modal-content').height(jQuery(window).height());
			jQuery('#parliament').removeClass('animated custom-load-time fadeIn fadeOut');
			console.log('pankaj');
		});
	});

	jQuery('.parliament-gallery-btn .photo-gallery-btn').click(function(){
		jQuery('body').on('shown.bs.modal', function () {
			jQuery('#diplomatgallery .modal-content').height(jQuery(window).height());
			jQuery('#speechs').removeClass('animated custom-load-time fadeIn fadeOut');
			console.log('pankaj');
		});
	});

	if( jQuery(window).width() < 768 ) {
		jQuery('#diplomat').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#parliament').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#speechs').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#government').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#charity').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#family').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#footer').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
	}


	if( jQuery(this).width() < 1170 ) { 
		window.onorientationchange = function(){
			window.location.reload();
		}
	}

});


jQuery(window).resize(function() {
	if( jQuery(this).width() > 1920 ) {
		//jQuery('.header-menu .modal-content').height(jQuery(window).height());
	}

   	if( jQuery(this).width() > 768 && jQuery(this).width() < 991 ) {
		// code
		jQuery('header .home-header').height(jQuery(window).height() );
	}

	if( jQuery(this).width() > 320 && jQuery(this).width() < 480 ) {
		// code
		jQuery('footer .footer-banner').height(jQuery(window).height() * 2.5);
	
	} else if( jQuery(this).width() > 481 && jQuery(this).width() < 767 ) {
		// code
		jQuery('footer .footer-banner').height(jQuery(window).height() * 2.4);
	
	} else if( jQuery(this).width() > 768 && jQuery(this).width() < 991 ) {
		// code
		jQuery('footer .footer-banner').height(jQuery(window).height() * 1.8);
	
	} else if( jQuery(this).width() > 992 && jQuery(this).width() < 1199 ) {
		// code
		jQuery('footer .footer-banner').height(jQuery(window).height() * 1.5);
	} else {
		jQuery('footer .footer-banner').height(jQuery(window).height() + 300);
	}


});



jQuery(window).load(function($){
	if( jQuery(this).width() > 991 ) {
		jQuery('.footer-social-wrapper').css('top',jQuery('footer .footer-banner').height()/12);
	} /*else if( jQuery(this).width() > 320 && jQuery(this).width() < 640 ) {
		jQuery('.footer-social-wrapper').css('top',jQuery('footer .footer-banner').height()/19);	
	} else if( jQuery(this).width() > 641 && jQuery(this).width() < 767 ) {
		jQuery('.footer-social-wrapper').css('top',jQuery('footer .footer-banner').height()/16);	
	} else if( jQuery(this).width() > 768 && jQuery(this).width() < 990 ) {
		jQuery('.footer-social-wrapper').css('top',jQuery('footer .footer-banner').height()/15);	
	}*/


	jQuery('body').on('shown.bs.modal', function () {
		if( jQuery(this).width() > 1600 ) {
			var headerval = jQuery('.modal-header').outerHeight();
			jQuery('.menu-model-body .model-content-wrapper').css('top', headerval);
		}else if( jQuery(this).width() > 1400 && jQuery(this).width() < 1600 ) {
			var headerval = jQuery('.modal-header').outerHeight();
			jQuery('.menu-model-body .model-content-wrapper').css('top', headerval + 20);
		} else if(jQuery(this).width() < 1400 ) {
			var headerval = jQuery('.modal-header').outerHeight();
			jQuery('.menu-model-body .model-content-wrapper').css('top', headerval);
		} else if(jQuery(this).width() < 768 ) {
			var headerval = jQuery('.modal-header').outerHeight();
			jQuery('.menu-model-body .model-content-wrapper').css('top', headerval);
		}

	});


	jQuery('.header-menu .modal-content').height(jQuery(window).height());

});

jQuery(document).ready(function($){
	var $root = $('html, body');
	jQuery('#primary-menu li a').click(function() {
	    $('#myModal').modal('hide');
	    $root.animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 2000);
	    return false;
	});
});

/* ========================= Diplomat section slider Js ========================= */

function diplomatslider(){
	// Diploment Tab Gallery slider
	jQuery('body').on('shown.bs.modal', function () {
		var galleryTop = new Swiper('.thumbnail-gallery-top', {
	        /*nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',*/
	        spaceBetween: 0,
	        speed: 800,
	    });
	    var galleryThumbs = new Swiper('.thumbnail-gallery-thumbs', {
	        spaceBetween: 0,
	        centeredSlides: true,
	        slidesPerView: 'auto',
	        touchRatio: 0.2,
	        slidesPerView: 6,
	        slideToClickedSlide: true,
	        speed: 800,
	        breakpoints: {
			// when window width is <= 480px
				767: {
				  	slidesPerView: 3,
				  	spaceBetween: 10,
					slidesPerGroup: 1,
					centeredSlides: true,
				},
				480: {
				  	slidesPerView: 2,
				  	spaceBetween: 10,
					slidesPerGroup: 1,
				},
				// when window width is <= 640px
				640: {
					slidesPerView: 2,
					spaceBetween: 10,
					slidesPerGroup: 1,
				}
			}
	    });
	    galleryTop.params.control = galleryThumbs;
	    galleryThumbs.params.control = galleryTop;

	    galleryTop.update(true);
	    galleryThumbs.update(true);	  
	});

	// Parliament Tab Gallery slider
	jQuery('body').on('shown.bs.tab', function () {
		var _galleryTop = new Swiper('.parliament-gallery-top', {
	        /*nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',*/
	        spaceBetween: 0,
	        speed: 800,
	    });
	    var _galleryThumbs = new Swiper('.parliament-gallery-thumbs', {
	        spaceBetween: 0,
	        centeredSlides: true,
	        slidesPerView: 'auto',
	        touchRatio: 0.2,
	        slidesPerView: 6,
	        slideToClickedSlide: true,
	        speed: 800,
	        breakpoints: {
	        	767: {
					slidesPerView: 3,
					spaceBetween: 10,
					slidesPerGroup: 1,
					centeredSlides: true,
				},
			// when window width is <= 480px
				480: {
				  	slidesPerView: 2,
				  	spaceBetween: 10,
					slidesPerGroup: 1,
				},
				// when window width is <= 640px
				640: {
					slidesPerView: 2,
					spaceBetween: 10,
					slidesPerGroup: 1,
				}
			}
	    });
	    _galleryTop.params.control = _galleryThumbs;
	    _galleryThumbs.params.control = _galleryTop;

	    _galleryTop.update(true);
	    _galleryThumbs.update(true);	  
	});
}

jQuery(document).ready(function(){

	// Diplomate Tab Image slider
	var swiper = new Swiper('.diplomat-slider', {
        slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 0,
        nextButton: '.diplomat-next',
        prevButton: '.diplomat-prev',
        speed: 800,
        // Responsive breakpoints
		breakpoints: {
			// when window width is <= 480px
			768: {
			  	slidesPerView: 2,
			  	spaceBetween: 0,
				slidesPerGroup: 1,
			},
			// when window width is <= 480px
			480: {
			  	slidesPerView: 1,
			  	spaceBetween: 0,
				slidesPerGroup: 1,
			},
			// when window width is <= 640px
			640: {
				slidesPerView: 1,
				spaceBetween: 0,
				slidesPerGroup: 1,
			}
		}
    });

	// Recent speech video slider
    var swiper = new Swiper('.speeches-slider-container	', {
        nextButton: '.speeches-next',
        prevButton: '.speeches-prev',
        slidesPerView: 1,
        paginationClickable: false,
        preventClicks: false,
        spaceBetween: 30,
        loop: true,
        speed: 800,
    });

    diplomatslider();

	jQuery('body').on('show.bs.modal', function () {
		diplomatslider();
	});

	jQuery('body').on('shown.bs.modal', function () {
		diplomatslider();
	});
	
	jQuery('body').on('shown.bs.tab', function () {
		diplomatslider();
	});
	
	jQuery('body').on('show.bs.tab', function () {
		diplomatslider();
	});

   
});



function parliamentslider(){
    
    /* Parliment photo gallery */ 
	jQuery('body').on('shown.bs.modal', function () {
		var galleryTop_ = new Swiper('.parliament-tab-gallery', {
		    nextButton: '.swiper-button-next',
		    prevButton: '.swiper-button-prev',
		    spaceBetween: 0,
		    speed: 800,
		});
		var galleryThumbs_ = new Swiper('.parliament-tab-thumbs', {
		    spaceBetween: 0,
		    centeredSlides: true,
		    slidesPerView: 'auto',
		    touchRatio: 0.2,
		    slidesPerView: 6,
		    slideToClickedSlide: true,
		    speed: 800,
		    breakpoints: {
			// when window width is <= 480px
				767: {
				  	slidesPerView: 3,
				  	spaceBetween: 10,
					slidesPerGroup: 1,
				},
				480: {
				  	slidesPerView: 2,
				  	spaceBetween: 10,
					slidesPerGroup: 1,
				},
				// when window width is <= 640px
				640: {
					slidesPerView: 2,
					spaceBetween: 10,
					slidesPerGroup: 1,
				}
			}
		});
		galleryTop_.params.control = galleryThumbs_;
		galleryThumbs_.params.control = galleryTop_;

		galleryTop_.update(true);
		galleryThumbs_.update(true);	  
	});

	// Parliament Tab Gallery slider
	jQuery('body').on('shown.bs.tab', function () {
		var _galleryTop_ = new Swiper('.dipolomat-tab-gallery', {
		    nextButton: '.swiper-button-next',
		    prevButton: '.swiper-button-prev',
		    spaceBetween: 0,
		    speed: 800,
		});
		var _galleryThumbs_ = new Swiper('.diplomat-tab-thumbs', {
		    spaceBetween: 0,
		    centeredSlides: true,
		    slidesPerView: 'auto',
		    touchRatio: 0.2,
		    slidesPerView: 6,
		    slideToClickedSlide: true,
		    speed: 800,
		    breakpoints: {
		    	767: {
				  	slidesPerView: 3,
				  	spaceBetween: 10,
					slidesPerGroup: 1,
				},
			// when window width is <= 480px
				480: {
				  	slidesPerView: 2,
				  	spaceBetween: 10,
					slidesPerGroup: 1,
				},
				// when window width is <= 640px
				640: {
					slidesPerView: 2,
					spaceBetween: 10,
					slidesPerGroup: 1,
				}
			}
		});
		_galleryTop_.params.control = _galleryThumbs_;
		_galleryThumbs_.params.control = _galleryTop_;

		_galleryTop_.update(true);
		_galleryThumbs_.update(true);
	});
}

/* ++++++++++++++++++++++++ Parliment Section Slider jquery ++++++++++++++++++++++++ */
jQuery(document).ready(function(){
	// Parliament Section timeline Section slider
	var swiper = new Swiper('.timeline-slider', {
      pagination: '.timeline-scrollbar',
      nextButton: '.parliament-next',
      prevButton: '.parliament-prev',
      scrollbarHide: false,
      slidesPerView: 'auto',
      paginationType: 'progress',
	 	centeredSlides: true,
	 	slidesOffsetBefore: -300,
		paginationClickable: true,
		spaceBetween: 30,		
      // Responsive breakpoints
		breakpoints: {
			// when window width is <= 768px
			768: {
				slidesPerView: 3,
				spaceBetween: 0,
				slidesPerGroup: 1,
				slidesOffsetBefore: 0,
				spaceBetween: 30,
			},
			// when window width is <= 640px
			640: {
				slidesPerView: 1,
				spaceBetween: 10,
				slidesPerGroup: 1,				
				slidesOffsetBefore: 20,
				slidesOffsetAfter: 20,
				spaceBetween: 0,
			},
			// when window width is <= 480px
			480: {
			  	slidesPerView: 1,
			  	spaceBetween: 10,
				slidesPerGroup: 1,				
				slidesOffsetBefore: 20,
				spaceBetween: 0,
			},
		}
    });

    // Parliament section Image slider
	var swiper = new Swiper('.parliament-slider', {
	    slidesPerView: 4,
	    paginationClickable: true,
	    spaceBetween: 0,
	    nextButton: '.parliament-slider-next',
	    prevButton: '.parliament-slider-prev',
        speed: 800,
		// Responsive breakpoints
		breakpoints: {
			// when window width is <= 768px
			768: {
			  slidesPerView: 2,
			  spaceBetween: 0,
			  slidesPerGroup: 1,
			},
			// when window width is <= 640px
			640: {
			  slidesPerView: 2,
			  spaceBetween: 0
			},
			// when window width is <= 480px
			480: {
			  slidesPerView: 1,
			  spaceBetween: 0,
			}
		}
	});

	// Key Speeches Slider
	var swiper = new Swiper('.key_speeches', {
        nextButton: '.key-speeches-next',
        prevButton: '.key-speeches-prev',
        pagination: '.key-speeches-pagination',
        paginationType: 'fraction',
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 30,
        slidesPerGroup: 3,
        preventClicks: false,
        preventClicksPropagation: false,
        speed: 800,
        // Responsive breakpoints
		breakpoints: {
			// when window width is <= 480px
			480: {
			  	slidesPerView: 1,
			  	spaceBetween: 20,
				slidesPerGroup: 1,
			},
			// when window width is <= 640px
			640: {
				slidesPerView: 1,
				spaceBetween: 20,
				slidesPerGroup: 1,
			}
		}
    });

/* *************************************************************************************************************************
													Parliament Photo Gallery JS
**************************************************************************************************************************** */ 
	
	parliamentslider();

	jQuery('body').on('show.bs.modal', function () {
		parliamentslider();
	});

	jQuery('body').on('shown.bs.modal', function () {
		parliamentslider();
	});
	
	jQuery('body').on('shown.bs.tab', function () {
		parliamentslider();
	});

	jQuery('body').on('show.bs.tab', function () {
		parliamentslider();
	});		

});

jQuery(window).load(function() {
	if( jQuery(window).width() > 768 && jQuery(window).width() <= 1920 ) {
		jQuery(".loader").fadeOut("slow");	
	}
});
// Animation scroll on specific section  
jQuery(document).ready(function($){	

	
	if( jQuery(window).width() >= 768 && jQuery(window).width() <= 1920 ) {
		// console.log('in > 768 and < 1920');		
		jQuery(window).scroll(function() {
			// Diplomat
	      jQuery('#diplomat:in-viewport(800)').removeClass('fadeOut').addClass('animated fadeIn custom-load-time');
	      // jQuery('#diplomat:in-viewport(800)').addClass('animated fadeIn custom-load-time');
	      jQuery('#diplomat:in-viewport(700)').find('.section-title').addClass('animated fadeInDown custom-load-time');
	      jQuery('#diplomat:in-viewport(700)').find('.custom-recent-tweets').addClass('animated fadeInDown custom-load-time');
	      jQuery('#diplomat:in-viewport(500)').find('.first-tweet').addClass('animated fadeInLeft custom-load-time');
	      jQuery('#diplomat:in-viewport(500)').find('.second-tweet').addClass('animated fadeInUp custom-load-time');
	      jQuery('#diplomat:in-viewport(500)').find('.latest-tweets li:nth-child(3)').addClass('animated fadeInRight custom-load-time');
	      jQuery('#diplomat:in-viewport(450)').find('.diplomat-slider').addClass('animated fadeInLeft custom-loader-time');
	      jQuery('#diplomat:in-viewport(400)').find('.speeches-section-wrapper').addClass('animated fadeInRight custom-loader-time');
	      
			// Resent Speeches
	      // jQuery('.speeches-section-wrapper:in-viewport(600)').addClass('animated fadeIn custom-load-time');
	      // jQuery('.speeches-section-wrapper:in-viewport(500)').find('.speeche-video').addClass('animated fadeInRight custom-load-time');
	    
			// Parliment
	      jQuery('#parliament:in-viewport(800)').removeClass('fadeOut').addClass('animated fadeIn custom-load-time');
	      jQuery('#parliament:in-viewport(800)').find('.section-title').addClass('animated fadeInDown custom-load-time');
	      jQuery('#parliament:in-viewport(800)').find('.custom-sub-title').addClass('animated fadeInDown custom-load-time');
	      jQuery('#parliament:in-viewport(650)').find('.parliament-horizantal-timeline').addClass('animated fadeInLeft custom-load-time');
	      jQuery('#parliament:in-viewport(450)').find('.parliament-section-slider').addClass('animated fadeInRight custom-load-time');

			// Key Speeches
	      jQuery('#speechs:in-viewport(900)').removeClass('fadeOut').addClass('animated fadeInRight custom-load-time');
			
			// Government
	      jQuery('#government:in-viewport(800)').removeClass('fadeOut').addClass('animated fadeIn custom-load-time');
	      jQuery('#government:in-viewport(800)').find('.section-title').addClass('animated fadeInDown custom-load-time');
	      jQuery('#government:in-viewport(800)').find('.timeline-main-title').addClass('animated fadeInDown custom-load-time');
	      jQuery('#government:in-viewport(700)').find('.1').addClass('animated slideInLeft custom-load-time');
	      jQuery('#government:in-viewport(600)').find('.2').addClass('animated slideInRight custom-load-time');
	      jQuery('.3:in-viewport(1000)').addClass('animated slideInLeft custom-load-time');
	      jQuery('.4:in-viewport(1000)').addClass('animated slideInRight custom-load-time');
	      jQuery('.5:in-viewport(1050)').addClass('animated slideInLeft custom-load-time');
	      jQuery('.6:in-viewport(1050)').addClass('animated slideInRight custom-load-time');
	  //     jQuery('.2:in-viewport(700)').addClass('animated slideInRight custom-load-time');
			// jQuery('.3:in-viewport(700)').addClass('animated slideInLeft custom-load-time');
	  //     jQuery('.4:in-viewport(700)').addClass('animated slideInRight custom-load-time');
			// jQuery('.5:in-viewport(700)').addClass('animated slideInLeft custom-load-time');
	  //     jQuery('.6:in-viewport(700)').addClass('animated slideInRight custom-load-time');
			
			// Charity
			jQuery('#charity:in-viewport(900)').removeClass('fadeOut').addClass('animated fadeIn custom-load-time');
	      jQuery('#charity:in-viewport(900)').find('.section-title').addClass('animated fadeInDown custom-load-time');
	      jQuery('#charity:in-viewport(700)').find('.charity-wrapper div:nth-child(1)').addClass('animated fadeInLeft custom-load-time');
	      jQuery('#charity:in-viewport(700)').find('.charity-wrapper div:nth-child(2)').addClass('animated fadeInRight custom-load-time');
			
			// Family
			jQuery('#family:in-viewport(900)').removeClass('fadeOut').addClass('animated fadeIn custom-load-time');
			// jQuery('#family:in-viewport(900)').addClass('animated fadeIn custom-load-time');
			jQuery('#family:in-viewport(900)').find('.section-title').addClass('animated fadeInDown custom-load-time');
			jQuery('#family:in-viewport(600)').find('.family-tab-section').addClass('animated slideInUp custom-load-time');
			jQuery('#family:in-viewport(600)').find('.family-img1').addClass('animated fadeInLeft custom-load-times');
			jQuery('#family:in-viewport(650)').find('.family-img2').addClass('animated fadeInLeft custom-load-times');
			jQuery('#family:in-viewport(600)').find('.family-img3').addClass('animated fadeInRight custom-load-times');
			jQuery('#family:in-viewport(650)').find('.family-img4').addClass('animated fadeInRight custom-load-times');
			
			// Footer
			jQuery('#footer:in-viewport(900)').removeClass('fadeOut').addClass('animated fadeIn custom-load-time');
			// jQuery('#footer:in-viewport(550)').addClass('animated fadeIn custom-load-time');
			jQuery('#footer:in-viewport(700)').find('.footer-heading').addClass('animated slideInDown custom-load-time');
			jQuery('#footer:in-viewport(700)').find('.footer-branding').addClass('animated slideInLeft custom-load-time');
			jQuery('#footer:in-viewport(700)').find('.footer-social').addClass('animated slideInRight custom-load-time');
	    });
	}


	if( jQuery(window).width() < 768 ) {
		jQuery('#diplomat').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#parliament').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#speechs').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#government').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#charity').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#family').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
		jQuery('#footer').removeClass('fadeOut').removeClass('animated').removeClass('custom-load-time');
	}

});
jQuery(window).load(function($){
	if( jQuery(window).width() >= 768 && jQuery(window).width() <= 1920 ) {
   		jQuery('.position-text:in-viewport()').addClass('animated fadeInLeft custom-load-time');
   		jQuery('.banner-heading:in-viewport()').addClass('animated fadeInUp custom-load-time');
	}
	// jQuery('#diplomat:in-viewport()').isInViewport({ tolerance: 100 }).css( 'background-color', 'red' );
});
