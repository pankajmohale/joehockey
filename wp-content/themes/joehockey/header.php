<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); 
global $redux_demo;
?>
<link rel="shortcut icon" href="<?php echo $redux_demo['favicon-image']['url']; ?>" type="image/x-icon" />
</head>

<body <?php body_class(); ?>>
<!-- <div class="loader"><h1 class="animated fadeIn custom-loader-time infinite">Joe Hockey</h1></div> -->
<div id="page" class="site">
	<header id="header" class="main-header" role="banner">
		<div class="site-branding">
			<?php
			if ( is_front_page() ) : ?>
			<div class="home-header" style="background-image: url(<?php echo $redux_demo['home-banner-media']['url']; ?>);">
				<div class="container-fluid">
					<div class="row">
						<div class="header-top-bar clearfix">
							<div class="col-xs-10 col-sm-11 col-md-11 header-logo-padding">
								<?php if (!$redux_demo['site-logo']['url']) { ?>
									<div class="logo">
										<img src="<?php echo $redux_demo['site-logo']['url']; ?>">
									</div>
								<?php } else { ?>
									<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo $redux_demo['banner-site-name']; ?></a></h1>
								<?php } ?>
							</div>
							<div class="col-xs-2 col-sm-1 col-md-1 header-btn-padding">
								<div class="menu-icon-btn">
									<div class="model-btn" data-toggle="modal" data-target="#myModal"><!-- <i class="fa fa-bars" aria-hidden="true"></i> --><img src="<?php echo get_template_directory_uri(); ?>/img/menu-icon.png" /></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="header-top-position-bar clearfix">
							<div class="col-md-3 col-md-offset-9">
								<?php if ($redux_demo['home-banner-subtitle']) { ?>
									<div class="position-text">
										<p><?php echo $redux_demo['home-banner-subtitle']; ?></p>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="header-heading clearfix">
							<div class="col-md-12">
								<?php if ($redux_demo['home-banner-title']) { ?>
									<div class="banner-heading">
										<h1><?php echo $redux_demo['home-banner-title']; ?></h1>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row">
						<div class="scroll_down_option">
							<div class="col-xs-1 col-sm-6">
								<div class="next_section_scroll next-section-loader animated infinite bounce">
									<div class="scroll_down">
										<span><img src="<?php echo get_template_directory_uri(); ?>/img/scroll-arrow.png"></span>scroll down 
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="social-icon clearfix">
									<div class="social-icon-wrapper">
										<?php if ($redux_demo['facebook-links']) { ?>
											<span class="facebook-links">
												<a href="<?php echo $redux_demo['facebook-links']; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
											</span>
										<?php } ?>
										<?php if ($redux_demo['twitter-links']) { ?>
											<span class="twitter-links">
												<a href="<?php echo $redux_demo['twitter-links']; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
											</span>
										<?php } ?>
										<?php if ($redux_demo['youtube-links']) { ?>
											<span class="youtube-links">
												<a href="<?php echo $redux_demo['youtube-links']; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
											</span>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php else : ?>
				<div class="container-fluid">
					<div class="row">
						<div class="header-top-bar inner-page inner-page clearfix">
							<div class="col-xs-11 col-sm-11 col-md-11">
								<?php if (!$redux_demo['site-logo']['url']) { ?>
									<div class="logo">
										<img src="<?php echo $redux_demo['site-logo']['url']; ?>">
									</div>
								<?php } else { ?>
									<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo $redux_demo['banner-site-name']; ?></a></h1>
								<?php } ?>
							</div>
							<div class="col-xs-1 col-sm-1 col-md-1">
								<div class="menu-icon-btn">
									<div class="model-btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-bars" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php
			endif;

			/*$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) :*/ ?>
				<!-- <p class="site-description"><?php //echo $description; /* WPCS: xss ok. */ ?></p> -->
			<?php
			// endif; ?>
		</div><!-- .site-branding -->

		<!-- Popup menu section -->
		<div class="header-menu-wrapper">
			<div class="dialog-menu-wrapper"><!-- Menu Model wrapper -->
				<!-- Button trigger modal -->
				<!-- Modal -->
				<div class="modal fade menu-modal-box" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog header-menu" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
					    <div class="col-xs-10 col-sm-10 col-md-10">
						    <!-- <h4 class="modal-title" id="myModalLabel">Modal title</h4> -->
						    <?php if (!$redux_demo['site-logo']['url']) { ?>
								<div class="logo">
									<img src="<?php echo $redux_demo['site-logo']['url']; ?>">
								</div>
							<?php } else { ?>
								<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo $redux_demo['banner-site-name']; ?></a></h1>
							<?php } ?>

						</div>
				      	<div class="col-xs-2 col-sm-2 col-md-2">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>			      		
				      	</div>
				      </div>
				      <div class="modal-body menu-model-body">
				      	<div class="model-content-wrapper">
				      		<div class="container">
				      			<div class="row">
							      	<div class="col-md-3 col-md-offset-9">
										<?php if ($redux_demo['home-banner-subtitle']) { ?>
											<div class="position-text title-white">
												<p><?php echo $redux_demo['home-banner-subtitle']; ?></p>
											</div>
										<?php } ?>
									</div>				      				
				      			</div>
				      		</div>
					        <div class="container">
				      			<div class="row">
							      	<div class="col-md-4">
									</div>
									<div class="col-md-4">
										<nav id="site-navigation" class="main-navigation" role="navigation">
											<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', '_s' ); ?></button> -->
											<?php 
											wp_nav_menu( array( 'theme_location' => 'primary_menu', 'menu_id' => 'primary-menu' ) ); ?>
										</nav><!-- #site-navigation -->
									</div>
									<div class="col-md-4">
									</div>

				      			</div>
				      		</div>
				      		<div class="blog-link-wrapper">
					      		<div class="container">
					      			<div class="row">
								      	<div class="col-md-4">
								      		<div class="blog-link">
												<span class="span-line"></span><a href="#" class="blog-url">Blog</a> 
								      		</div>
										</div>
										<div class="col-md-4">
										</div>
										<div class="col-md-4">
										</div>

					      			</div>
					      		</div>				      			
				      		</div>
					        
				      	</div>
				      </div>
				      <div class="modal-footer menu-social-share">
<!-- 				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary">Save changes</button> -->
				        <div class="social-icon menu-social-icon clearfix">
							<div class="menu-social-icon-wrapper">
								<span class="join-text">
				        			<span class="">Join Me </span>
								</span>
								<?php if ($redux_demo['facebook-links']) { ?>
									<span class="facebook-links">
										<a href="<?php echo $redux_demo['facebook-links']; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
									</span>
								<?php } ?>
								<?php if ($redux_demo['twitter-links']) { ?>
									<span class="twitter-links">
										<a href="<?php echo $redux_demo['twitter-links']; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
									</span>
								<?php } ?>
								<?php if ($redux_demo['youtube-links']) { ?>
									<span class="youtube-links">
										<a href="<?php echo $redux_demo['youtube-links']; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
									</span>
								<?php } ?>
							</div>
						</div>
				      </div>
				    </div>
				  </div>
				</div>
			</div><!-- Menu Model Wrapper End -->
		</div>

	</header><!-- #masthead -->

	<div id="content-home" class="site-content">
	
	<?php if ( !is_front_page() && !is_home() ) : ?>
		<div id="content" class="site-content">
		<!-- <div class="breadcrumb"><?php get_breadcrumb(); ?></div> -->
	<?php endif; ?>