<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
global $redux_demo;
?>

	</div><!-- #content -->

	<footer id="footer" class="main-footer animated fadeOut custom-load-time" role="contentinfo">
	<?php if ( is_front_page() ) : ?>
		<div class="footer-banner" style="background-image: url(<?php echo $redux_demo['home-footer-banner']['url']; ?>);">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="footer-social-wrapper">
							<div class="footer-heading">
								<div class="footer-about-joe-hockey">
									<div class="row">
										<div class="col-sm-12 col-xs-12">
											<?php echo $redux_demo['footer-about-joehockey-title']; ?>
										</div>										
									</div>
									<div class="row">
										<div class="col-sm-4 col-xs-12">
											<img src="<?php echo $redux_demo['footer-about-joehockey-image']['url']; ?>"  class="img-responsive"/>
										</div>
										<div class="col-sm-4 col-xs-12">
											<?php echo $redux_demo['footer-about-joehockey']; ?>
										</div>
										<div class="col-sm-4 col-xs-12">
											<?php echo $redux_demo['footer-about-joehockey-2']; ?>
										</div>
									</div>
								</div>
								<div><?php echo $redux_demo['footer-banner-subtitle']; ?></div>
							</div>
							<div class="footer-branding"><div><?php echo $redux_demo['footer-banner-title']; ?></div></div>
							<div class="footer-social">
								<div class="footer-social-icon clearfix">
									<div class="footer-social-icon-wrapper">
										<?php if ($redux_demo['facebook-links']) { ?>
											<span class="facebook-links">
												<a href="<?php echo $redux_demo['facebook-links']; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
											</span>
										<?php } ?>
										<?php if ($redux_demo['twitter-links']) { ?>
											<span class="twitter-links">
												<a href="<?php echo $redux_demo['twitter-links']; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
											</span>
										<?php } ?>
										<?php if ($redux_demo['youtube-links']) { ?>
											<span class="youtube-links">
												<a href="<?php echo $redux_demo['youtube-links']; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
											</span>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom-bar">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 col-md-2">
						<div class="footer-logo">
							<a href="<?php echo site_url(); ?>"><?php echo $redux_demo['footer-copyright-title']; ?></a>
						</div>
					</div>
					<div class="col-sm-5 col-md-7">
						<div class="footer-copyright">
							<?php echo $redux_demo['footer-copyright-text']; ?>
						</div>
					</div>
					<div class="col-sm-4 col-md-3">
						<div class="pull-right">
							<nav id="footer-navigation" class="main-navigation">
								<?php wp_nav_menu( array( 'theme_location' => 'footer_menu', 'menu_id' => 'footer-menu' ) ); ?>
							</nav>							
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php else : ?>
		<div class="footer-bottom-bar inner-pages">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 col-md-2">
						<div class="footer-logo">
							<a href="<?php echo site_url(); ?>"><?php echo $redux_demo['footer-copyright-title']; ?></a>
						</div>
					</div>
					<div class="col-sm-5 col-md-7">
						<div class="footer-copyright">
							<?php echo $redux_demo['footer-copyright-text']; ?>
						</div>
					</div>
					<div class="col-sm-4 col-md-3">
						<div class="pull-right">
							<nav id="footer-navigation" class="main-navigation">
								<?php wp_nav_menu( array( 'theme_location' => 'footer_menu', 'menu_id' => 'footer-menu' ) ); ?>
							</nav>							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', '_s' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', '_s' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', '_s' ), '_s', '<a href="https://automattic.com/" rel="designer">Automattic</a>' ); ?>
		</div> -->
		<!-- .site-info -->
	<?php endif; ?>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
