<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<!-- <main id="main" class="site-main" role="main"> -->
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-3 col-md-3">
					<?php get_sidebar(); ?>					
				</div>
				<div class="col-xs-12 col-sm-9 col-md-9">
					<?php
					while ( have_posts() ) : the_post();

				        echo '<div class="post-pagination clearfix">';
							the_post_navigation( array(
					            'prev_text'                  => __( '<span class="back-arrow"><img src="'. get_template_directory_uri().'/img/side-arrow.png" /></span> Back' ),
					            'next_text'                  => __( 'Front <span class="front-arrow"><img src="'. get_template_directory_uri().'/img/side-arrow.png" /></span>' ),
					        ) );
				        echo '</div>';

						get_template_part( 'template-parts/content', get_post_format() );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>
				</div>
			</div>
		</div>
		<!-- </main> --><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
