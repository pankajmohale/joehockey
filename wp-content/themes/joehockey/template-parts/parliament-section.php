<?php
/**
 * Template part for displaying parliament section
 */
global $redux_demo;
?>
<div class="home-section-content bg-navy-blue sections-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title-wrapper">
					<h2 class="section-title title-white text-center"><?php echo $redux_demo['home-section-second-title']; ?></h2>
					<h4 class="title-white text-center custom-sub-title">Service and achievements</h4>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<?php 
				$parliament_slider_args = array(
					'post_type' => 'post',
					'posts_per_page' => -1,
					'tax_query' => array(
						array(
							'taxonomy' => 'gallery_type',
							'field'    => 'slug',
							'terms'    => 'parliament',
						),
					),
					'meta_query' => array(
				        'state_clause' => array(
				            'key' => 'add_in_timeline',
				            'value' => 'yes',
				        ),
				    ),
					'orderby' => 'date',
					'order'   => 'ASC',
				);
				// the query
			$parliament_slider_the_query = new WP_Query( $parliament_slider_args ); ?>

			<div class="parliament-horizantal-timeline">
				<div class="timeline-wrapper">
				<?php if ( $parliament_slider_the_query->have_posts() ) : ?>

				<!-- pagination here -->
					<div class="swiper-container timeline-slider">
				        <div class="swiper-wrapper swiper-timeline-slider">
				            <!-- the loop -->
							<?php while ( $parliament_slider_the_query->have_posts() ) : $parliament_slider_the_query->the_post(); ?>
				            		<div class="swiper-slide">
				            			<div class="timeline-post-meta">
												<div class="timeline-year">
				            				<?php $timeline_year = get_the_date('Y'); 
				            					if ( $timeline_year != $temp_year) {				            						
				            				?>
													<h1 class="timeline-date-title"><?php the_date('Y'); ?></h1>
											<?php 
												} else { 
											?>
												<h1 class="timeline-date-title">&nbsp;</h1>
											<?php }
												$temp_year = $timeline_year; 
											?>
												</div>
											<?php $view_mon = get_the_date('F');
												if ($view_mon != $temp_date) { ?>
							            			<div class="timeline-dot-wrapper">
														<span class="timeline-date-title">&nbsp;</span>
													</div>
													<div class="timeline-month">
														<h3 class="timeline-date-title">
															<?php echo get_the_date('F'); ?>
														</h3>
													</div>
												<?php } 
												$temp_date = $view_mon; 
											?>				            				
				            			</div>
				            			<div class="dot-wrapper-">
										</div>
				            			<div class="slider-content-part">
											<?php the_excerpt(); ?>				            			
				            			</div>
				            		</div>
							<?php endwhile; ?>
							<!-- end of the loop -->
				        </div>
				        <!-- Add Scrollbar -->
				        <div class="swiper-scrollbar timeline-scrollbar"></div>
				        <!-- Navigation -->
				        <div class="swiper-button-next parliament-next swiper-button-white"></div>
				        <div class="swiper-button-prev parliament-prev swiper-button-white"></div>
				    </div>		
				<!-- pagination here -->

				<?php wp_reset_postdata(); ?>

				<?php else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
		$parliament_image_args = array(
			'post_type' => 'post',
			'tax_query' => array(
				array(
					'taxonomy' => 'gallery_type',
					'field'    => 'slug',
					'terms'    => 'parliament',
				),
			),
			'orderby' => 'date',
			'order'   => 'ASC',
		);
		// the query
	$parliament_image_the_query = new WP_Query( $parliament_image_args ); ?>
<div class="parliament-section-slider">
	<div class="container-fluid">
		<div class="row">
			<div class="swiper-container parliament-slider padding-bottom">
		        <div class="swiper-wrapper">
		        	<?php if ( $parliament_image_the_query->have_posts() ) : ?>

					<!-- pagination here -->

					<!-- the loop -->
					<?php while ( $parliament_image_the_query->have_posts() ) : $parliament_image_the_query->the_post(); ?>
						<?php if ( has_post_thumbnail() ) { ?>
							<div class="swiper-slide">
								<img src="<?php echo the_post_thumbnail_url( 'scroller-gallery' ); ?>" class="img-responsive">
							</div>						    
						<?php } ?>
					<?php endwhile; ?>
					<!-- end of the loop -->

					<!-- pagination here -->

					<?php wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php _e( 'Sorry, no images avalilable.' ); ?></p>
				<?php endif; ?>
		        </div>
		        <!-- Add Pagination -->
		        <!-- <div class="swiper-pagination diplomat-pagination"></div> -->
		        <!-- Add Arrows -->
		        <div class="swiper-button-next parliament-slider-next swiper-button-white"></div>
		        <div class="swiper-button-prev parliament-slider-prev swiper-button-white"></div>
		    </div>		
		</div>	
	</div>	
</div>
