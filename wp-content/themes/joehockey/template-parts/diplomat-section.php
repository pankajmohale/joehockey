<?php
/**
 * Template part for displaying diplomat section
 */
global $redux_demo;
?>
<div class="home-section-content bg-sky-blue sections-padding padding-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title-wrapper">
					<h2 class="section-title title-white text-center "><?php echo $redux_demo['home-section-first-title']; ?></h2>
					<h4 class="title-white text-center custom-recent-tweets">Recent Tweets</h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="twitter-handle-wrapper">
					<h3 class="twitter-handle title-white"><i class="fa fa-twitter" aria-hidden="true"></i> @JoeHockey<?php //echo $redux_demo['home-section-first-title']; ?></h3>
				</div>
			</div>
		</div>
		<div class="row">
			<?php echo do_shortcode('[widget id="latest_tweets_widget-2"]'); ?>
		</div>
	</div>
</div>
<div class="diplomat-slider-wrapper">
	<?php 
	$diplomat_slider_args = array(
		'post_type' => 'post',
		'tax_query' => array(
			array(
				'taxonomy' => 'gallery_type',
				'field'    => 'slug',
				'terms'    => 'diplomat',
			),
		),
	);
	// the query
	$diplomat_slider_the_query = new WP_Query( $diplomat_slider_args ); ?>

	<div class="container-fluid">
		<div class="row">
			<div class="swiper-container diplomat-slider">
		        <div class="swiper-wrapper">
		        	<?php if ( $diplomat_slider_the_query->have_posts() ) : ?>

					<!-- pagination here -->

					<!-- the loop -->
					<?php while ( $diplomat_slider_the_query->have_posts() ) : $diplomat_slider_the_query->the_post(); ?>
						<?php if ( has_post_thumbnail() ) { ?>
							<div class="swiper-slide">
								<img src="<?php echo the_post_thumbnail_url( 'scroller-gallery' ); ?>" class="img-responsive">
							</div>						    
						<?php } ?>
					<?php endwhile; ?>
					<!-- end of the loop -->

					<!-- pagination here -->

					<?php wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
		        </div>
		        <!-- Add Pagination -->
		        <div class="swiper-pagination diplomat-pagination"></div>
		        <!-- Add Arrows -->
		        <div class="swiper-button-next diplomat-next"></div>
		        <div class="swiper-button-prev diplomat-prev"></div>
		    </div>		
		</div>
	</div>	
</div>