<?php
/**
 * Template part for displaying dialog menu
 */
global $redux_demo;
?>
<div class="dialog-menu-wrapper">
	<!-- Button trigger modal -->
	<!-- Modal -->
	<div class="modal fade menu-modal-box" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog header-menu" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
		    <div class="col-md-10">
			    <!-- <h4 class="modal-title" id="myModalLabel">Modal title</h4> -->
			    <?php if (!$redux_demo['site-logo']['url']) { ?>
					<div class="logo">
						<img src="<?php echo $redux_demo['site-logo']['url']; ?>">
					</div>
				<?php } else { ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo $redux_demo['banner-site-name']; ?></a></h1>
				<?php } ?>

			</div>
	      	<div class="col-md-2">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>			      		
	      	</div>
	      </div>
	      <div class="modal-body">
	        <nav id="site-navigation" class="main-navigation" role="navigation">
				<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', '_s' ); ?></button> -->
				<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
			</nav><!-- #site-navigation -->
	      </div>
	      <div class="modal-footer menu-social-share">
			<!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button> -->
	        <div class="social-icon menu-social-icon clearfix">
				<div class="menu-social-icon-wrapper">
					<span class="join-text">
	        			<span class="">Join Me <i class="fa fa-arrow-right" aria-hidden="true"></i></span>
					</span>
					<?php if ($redux_demo['facebook-links']) { ?>
						<span class="facebook-links">
							<a href="<?php echo $redux_demo['facebook-links']; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</span>
					<?php } ?>
					<?php if ($redux_demo['twitter-links']) { ?>
						<span class="twitter-links">
							<a href="<?php echo $redux_demo['twitter-links']; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</span>
					<?php } ?>
					<?php if ($redux_demo['youtube-links']) { ?>
						<span class="youtube-links">
							<a href="<?php echo $redux_demo['youtube-links']; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
						</span>
					<?php } ?>
				</div>
			</div>
	      </div>
	    </div>
	  </div>
	</div>
</div>