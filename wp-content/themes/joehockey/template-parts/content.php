<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<div class="entry-meta custom-post-meta">
			<?php custom_s_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title nive-medium custom-post-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<?php
		endif; ?>
	</div><!-- .entry-header -->
	<div class="entry-thumbnail">
		<div class="row">
			<div class="col-sm-8">
				<img src="<?php	the_post_thumbnail_url( 'gallery-slider' ); ?>" class="img-responsive"/>
			</div>
			<div class="col-sm-4">
				<h5 class="mont-regular post-thumbnail-title">
					<?php 
						$gallery_title = get_post_meta( get_the_ID(), 'gallery_title', true ); 
						echo $gallery_title;
					?>
				</h5>
			</div>
		</div>
	</div>
	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', '_s' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', '_s' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<div class="entry-footer">
		<?php //_s_entry_footer(); ?>
	</div><!-- .entry-footer -->
</article><!-- #post-## -->
