<?php
/**
 * Template part for displaying Charity Section
 */
global $redux_demo;
?>

	<?php 
		$charity_args = array(
			'post_type' => 'charity',
			'tax_query' => array(
				array(
					'taxonomy' => 'charity_types',
					'field'    => 'slug',
					'terms'    => 'foundations',
				),
			),
			'orderby' => 'date',
			'order'   => 'ASC',
		);
		// the query
		$charity_args_the_query = new WP_Query( $charity_args ); 
	?>
<div class="col-sm-6 col-md-3">
	<div class="foundation-title section-sub-title">
		<h2 class="charity-sub-title nive-medium">Foundation:</h2>
	</div>
	<?php if ( $charity_args_the_query->have_posts() ) : ?>

	<!-- pagination here -->

        <!-- the loop -->
		<?php while ( $charity_args_the_query->have_posts() ) : $charity_args_the_query->the_post(); ?>
			<div class="foundation-content-loop">
	        	<h3 class="subsection-post-title title-white nive-medium"><?php the_title(); ?></h3>
	        	<?php
	        		$meta = get_post_meta( get_the_ID(), 'video_link_visit_link', true );
				?>		       
	        	<a href="<?php echo $meta; ?>" class="navi-bold">Visit Site</a>   			
			</div>
        <?php endwhile; ?>
		<!-- end of the loop -->
	        	
	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>	
</div>
<div class="col-sm-6 col-md-6">
	<div class="foundation-title section-sub-title">
		<h2 class="charity-sub-title nive-medium">Events:</h2>
	</div>
	<?php 
		$charity_event_args = array(
			'post_type' => 'charity',
			'posts_per_page' => 4,
			'tax_query' => array(
				array(
					'taxonomy' => 'charity_types',
					'field'    => 'slug',
					'terms'    => 'events',
				),
			),
			'orderby' => 'date',
			'order'   => 'ASC',
		);
		// the query
		$charity_event_the_query = new WP_Query( $charity_event_args ); 
	?>
	<div class="row">
		<?php if ( $charity_event_the_query->have_posts() ) : ?>

		<!-- pagination here -->

	        <!-- the loop -->
			<?php while ( $charity_event_the_query->have_posts() ) : $charity_event_the_query->the_post(); ?>
	        	<div class="event-content-loop col-md-6">
		        	<h3 class="subsection-post-title title-white nive-medium"><?php the_title(); ?></h3>
		        	<?php
		        		$meta = get_post_meta( get_the_ID(), 'video_link_visit_link', true );
		        		$visit_link = get_post_meta( get_the_ID(), 'visit_link', true );
		        		if ($meta) { 
		        	?>
		        		<a href="<?php echo $meta; ?>" class="navi-bold">Watch Video</a>	            			
		        	<?php } elseif($visit_link) { ?>
		        		<a href="<?php echo $visit_link; ?>" class="navi-bold" target="_blank">Read more</a>	            			
		        	<?php } else { ?>
		        		<a href="<?php get_permalink(); ?>" class="navi-bold">Read more</a>	            			
 			        <?php }	?>

	        	</div>
	        <?php endwhile; ?>
			<!-- end of the loop -->
		        	
		<!-- pagination here -->

		<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
	</div>

</div>
