<?php
/**
 * Family Section template
 */
global $redux_demo;
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="section-title-wrapper">
				<h2 class="section-title title-navy-blue text-center"><?php echo $redux_demo['home-section-fifth-title']; ?></h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="hidden-xs hidden-sm col-md-2"></div>
		<div class="col-xs-12 col-md-8">
			<div class="family-tab-section">
				<ul class="nav nav-tabs nav-justified ">
				  <li class="family-menu1 active"><a data-toggle="tab" href="#home"><div><?php echo $redux_demo['family-section-1-tab-title']; ?></div></a></li>
				  <li class="family-menu2"><a data-toggle="tab" href="#menu1"><div><?php echo $redux_demo['family-section-2-tab-title']; ?></div></a></li>
				  <li class="family-menu3"><a data-toggle="tab" href="#menu2"><div><?php echo $redux_demo['family-section-3-tab-title']; ?></div></a></li>
				</ul>
				<div class="tab-content">
				  <div id="home" class="tab-pane fade in active">
				    <div class="tab-body-content">
				    	<p><?php echo $redux_demo['family-section-1-content']; ?> <span class="tab-read-link"><a href="<?php echo $redux_demo['family-section-1-tab-link']; ?>">Show More</a></span></p>
				    </div>
				  </div>
				  <div id="menu1" class="tab-pane fade">
				    <div class="tab-body-content">
				    	<p><?php echo $redux_demo['family-section-2-content']; ?> <span class="tab-read-link"><a href="<?php echo $redux_demo['family-section-2-tab-link']; ?>">Show More</a></span></p>
				    </div>
				  </div>
				  <div id="menu2" class="tab-pane fade">
				    <div class="tab-body-content">
				    	<p><?php echo $redux_demo['family-section-3-content']; ?> <span class="tab-read-link"><a href="<?php echo $redux_demo['family-section-3-tab-link']; ?>">Show More</a></span></p>
				    </div>
				  </div>
				</div>								
			</div>
		</div>
		<div class="hidden-xs hidden-sm col-md-2"></div>
	</div>
</div>
<div class="family-meida-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-3 col-md-3 no-padding">
				<div class="family-media family-img1">
					<img src="<?php echo $redux_demo['family-media-1']['url']; ?>" class="img-responsive">	
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 no-padding">
				<div class="family-media family-img2">
					<img src="<?php echo $redux_demo['family-media-2']['url']; ?>" class="img-responsive">					
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 no-padding">
				<div class="family-media family-img3">
					<img src="<?php echo $redux_demo['family-media-3']['url']; ?>" class="img-responsive">					
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 no-padding">
				<div class="family-media family-img4">
					<img src="<?php echo $redux_demo['family-media-4']['url']; ?>" class="img-responsive">					
				</div>
			</div>
		</div>						
	</div>						
</div>