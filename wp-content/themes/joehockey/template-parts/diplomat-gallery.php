<?php
/**
 * Template part for displaying diplomat Gallery section
 */
global $redux_demo;
?>
<div class="photo-gallery-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="diplomat-photo-gallery">
					<!-- Large modal -->
					<!-- Button trigger modal -->
					<div class="diplomat-gallery-btn text-center">
						<span class="top-line"></span>
						<a class="photo-gallery-btn" data-toggle="modal" data-target="#diplomatgallery">
						  More Photos
						</a>
					</div>

					<!-- Modal -->
					<div class="modal fade" id="diplomatgallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog photo-gallery-modal-box" role="document">
					    <div class="modal-content">
					      	<div class="modal-header"><!-- Model Header -->
							    <div class="col-xs-12 col-sm-5 col-md-6">
								    <?php if (!$redux_demo['site-logo']['url']) { ?>
										<div class="logo">
											<img src="<?php echo $redux_demo['site-logo']['url']; ?>">
										</div>
									<?php } else { ?>
										<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo $redux_demo['banner-site-name']; ?></a></h1>
									<?php } ?>
								</div>
						      	<div class="col-xs-12 col-sm-7 col-md-6">
							        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>			      		
						      		<ul class="nav nav-tabs pull-right model-gallery-nav">
									  	<li class="active"><a data-toggle="tab" href="#diplomat_tab">Diplomat</a></li>
									  	<li><a data-toggle="tab" href="#parliament_tab">Parliament</a></li>
									</ul>
						      	</div>
					      	</div><!-- Model Header -->
					      	<div class="modal-body"><!-- Model Body -->
						      	<div class="container-fluid"><!-- Model Container -->
						      		<div class="row">
						      			<!-- Tab start -->
									    <div class="tab-content">
											<div id="diplomat_tab" class="tab-pane fade in active">
												<!-- diplomat.gallerypopup.php -->
												<?php get_template_part( 'template-parts/popup-template/diplomat', 'gallerypopup' ); ?>
												<!-- diplomat End -->
											</div>
											<div id="parliament_tab" class="tab-pane fade">
												<!-- parliament.gallerypopup.php -->
												<?php get_template_part( 'template-parts/popup-template/parliament', 'gallerypopup' ); ?>
												<!-- parliament End -->
											</div>
										</div>
										<!-- Tab End -->									
						      		</div>								
								</div>					        
					      	</div><!-- Model Body -->
					    </div>
					  </div>
					</div><!-- Model End -->
				</div>
			</div>
		</div>		
	</div>
</div>