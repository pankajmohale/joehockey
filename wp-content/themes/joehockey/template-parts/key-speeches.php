<?php
/**
 * Template part for displaying Key Speeches section
 */
global $redux_demo;
?>
<div class="home-section-content bg-gray">
	<div class="container"> <!-- Container -->
		<div class="row">
			<div class="col-md-12">
				<div class="speech-section-wrapper">
					<h2 class="section-title title-white"><?php 
						$category_id = get_cat_ID( 'speeches' );
					    // Get the URL of this category
					    $category_link = get_category_link( $category_id );
					    echo "<a href='".esc_url( $category_link ) . "' title='Key Speeches'>Key Speeches</a>";
					?></h2>
				</div>
			</div>
		</div>
	</div> <!-- Container -->
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="key-speeches-wrapper">
					<?php 
						$key_speeches_arg = array(
							'post_type' => 'post',
							'posts_per_page' => 6,
							'tax_query' => array(
								array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => 'speeches',
								),
							),
							'meta_query' => array(
								array(
									'key'     => 'key_speeches',
									'value'   => 'yes',
									'compare' => 'LIKE',
								),
							),
						);
						// the query
						$key_speeches_the_query = new WP_Query( $key_speeches_arg ); 
					?>
					<?php if ( $key_speeches_the_query->have_posts() ) : ?>
						<!-- pagination here -->

						<!-- Swiper -->
					    <div class="swiper-container key_speeches">
					        <div class="swiper-wrapper">
								<!-- the loop -->
					        	<?php while ( $key_speeches_the_query->have_posts() ) : $key_speeches_the_query->the_post(); ?>
					            	<div class="swiper-slide">
											<div class="key-speeches-title">
												<h1><?php echo get_the_title(); ?></h1>
											</div>
											<div class="key-speeches-description">
												<p><?php the_excerpt_max_charlength(100); ?></p>
											</div>
											<div class="key-speeches-more-link">
												<a href="<?php echo get_post_permalink( get_the_ID() ); ?>">Read More</a>
											</div>
					            	</div>										
								<?php endwhile; ?>
								<!-- end of the loop -->
					        </div>
					        <!-- Add Pagination -->
					        <div class="swiper-pagination key-speeches-pagination"></div>
					        <!-- Add Arrows -->
					        <div class="swiper-button-next key-speeches-next"></div>
					        <div class="swiper-button-prev key-speeches-prev"></div>
					    </div>

						<!-- pagination here -->

						<?php wp_reset_postdata(); ?>

					<?php else : ?>
						<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
				</div>
			</div>			
		</div>		
	</div><!-- container -->
</div>