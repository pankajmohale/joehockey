<?php
/**
 * Template part for displaying Popup diplomat Gallery section
 */
global $redux_demo;
?>
<!-- Diplomat gallery popup -->
	<div class="diplomat-thumbnail-gallery">
			<?php 
				$diplomat_slider_args = array(
					'post_type' => 'post',
					'tax_query' => array(
						array(
							'taxonomy' => 'gallery_type',
							'field'    => 'slug',
							'terms'    => 'diplomat',
						),
					),
				);
				// the query
				$diplomat_slider_the_query = new WP_Query( $diplomat_slider_args ); 
			?>	
		<!-- Swiper -->
	    <div class="swiper-container thumbnail-gallery-top gallery-top">
	        <div class="swiper-wrapper">

	        	<?php if ( $diplomat_slider_the_query->have_posts() ) : ?>

				<!-- pagination here -->

				<!-- the loop -->
				<?php while ( $diplomat_slider_the_query->have_posts() ) : $diplomat_slider_the_query->the_post(); ?>
					<?php if ( has_post_thumbnail() ) { ?>
						<div class="swiper-slide">
							<div class="col-md-5 slide-left-border no-padding text-center">
								<img src="<?php echo the_post_thumbnail_url( 'gallery-slider' ); ?>" class="img-responsive">
							</div>
							<div class="col-md-6">
								<div class="slider-post-data">
									<?php 
										$categories = get_the_category();
										if ( ! empty( $categories ) ) {
											$category_name = '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
										}
										$post_date_ = get_the_date('M j, Y', get_the_ID());
										echo '<div class="post-meta">'.$post_date_ .' '. $category_name.'</div>';
										$gallery_title = get_post_meta( get_the_ID(), 'gallery_title', true );
										echo '<div class="slider-diplomat-post-title"><h2>'.$gallery_title.'</h2></div>';
									?>
								</div>
							</div>
						</div>						    
					<?php } ?>
				<?php endwhile; ?>
				<!-- end of the loop -->

				<!-- pagination here -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

	        </div>
	        <!-- Add Arrows -->
	        <!-- <div class="swiper-button-next swiper-button-white"></div>
	        <div class="swiper-button-prev swiper-button-white"></div> -->
	    </div>
	    <div class="swiper-container thumbnail-gallery-thumbs gallery-thumbs">
	        <div class="swiper-wrapper">
		        	<?php if ( $diplomat_slider_the_query->have_posts() ) : ?>

					<!-- pagination here -->

					<!-- the loop -->
					<?php while ( $diplomat_slider_the_query->have_posts() ) : $diplomat_slider_the_query->the_post(); ?>
						<?php if ( has_post_thumbnail() ) { ?>
							<div class="swiper-slide slide-right-border">
								<img src="<?php echo the_post_thumbnail_url( 'home-slider' ); ?>" class="img-responsive">
							</div>						    
						<?php } ?>
					<?php endwhile; ?>
					<!-- end of the loop -->

					<!-- pagination here -->

					<?php wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
	        </div>
	    </div>
	</div><!-- Diplomat gallery popup -->
											