<?php
/**
 * Template part for displaying Parliament Gallery section
 */
global $redux_demo;
?>
<div class="parliament-photo-gallery-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="parliament-gallery-section">
					<div class="parliament-gallery-btn text-center">
						<!-- Button trigger modal -->
						<a class="photo-gallery-btn" data-toggle="modal" data-target="#parliamentgallery">
						  More Photos
						</a>
					</div>					
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid"> <!-- Model Container -->
		<div class="row">
			<div class="col-xs-12">
				<div class="parliament-gallery-modal-box">
					<?php 
						$parliament_gallery_arg = array(
							'post_type' => 'post',
							'tax_query' => array(
								array(
									'taxonomy' => 'gallery_type',
									'field'    => 'slug',
									'terms'    => 'parliament',
								),
							),
						);
						// the query
						$parliament_gallery_photo_the_query = new WP_Query( $parliament_gallery_arg ); 
					?>
					<!-- Modal -->
						<div class="modal fade" id="parliamentgallery" tabindex="-1" role="dialog" aria-labelledby="parliamentgallery" aria-hidden="true">
					  		<div class="modal-dialog photo-gallery-modal-box" role="document">
							    <div class="modal-content">
							      	<div class="modal-header"><!-- Model Header -->
									    <div class="col-xs-12 col-sm-5 col-md-6">
										    <?php if (!$redux_demo['site-logo']['url']) { ?>
												<div class="logo">
													<img src="<?php echo $redux_demo['site-logo']['url']; ?>">
												</div>
											<?php } else { ?>
												<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo $redux_demo['banner-site-name']; ?></a></h1>
											<?php } ?>
										</div>
								      	<div class="col-xs-12 col-sm-7 col-md-6">
									        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">&times;</span>
									        </button>			      		
								      		<ul class="nav nav-tabs pull-right model-gallery-nav">
												  	<li role="presentation" class="active">
														<a href="#parliament_photo_gallery_tab" aria-controls="parliament_photo_gallery_tab" role="tab" data-toggle="tab">Parliament</a>
													</li>
													<li role="presentation">
														<a href="#parliament_photo_gallery_diplomat" aria-controls="parliament_photo_gallery_diplomat" role="tab" data-toggle="tab">Diplomat</a>
													</li>
												</ul>
								      	</div>
							      	</div><!-- Model Header -->
							      	<div class="modal-body">
							      		<!-- Tab panes -->
										  	<div class="tab-content">
										    	<div role="tabpanel" class="tab-pane active" id="parliament_photo_gallery_tab">
										   		<div class="parliament-section-parliament-gallery-wrapper">
										   			<!-- Swiper -->
														<div class="swiper-container parliament-tab-gallery">
															<div class="swiper-wrapper">
															<?php if ( $parliament_gallery_photo_the_query->have_posts() ) : ?>
																<!-- pagination here -->
																<!-- the loop -->
																<?php while ( $parliament_gallery_photo_the_query->have_posts() ) : $parliament_gallery_photo_the_query->the_post(); ?>
																	<?php if ( has_post_thumbnail() ) { ?>
																		<div class="swiper-slide">
																			<div class="col-md-5 slide-left-border no-padding text-center">
																				<img src="<?php echo the_post_thumbnail_url( 'gallery-slider' ); ?>" class="img-responsive">
																			</div>
																			<div class="col-md-6">
																				<div class="slider-post-data">
																				<?php 
																					$categories = get_the_category();
																					if ( ! empty( $categories ) ) {
																						$category_name = '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
																					}
																					$post_date_ = get_the_date('M j, Y', get_the_ID());
																					echo '<div class="post-meta">'.$post_date_ .' '. $category_name.'</div>';
																					$gallery_title = get_post_meta( get_the_ID(), 'gallery_title', true );
																					echo '<div class="slider-diplomat-post-title"><h2>'.$gallery_title.'</h2></div>';
																				?>																
																				</div>
																			</div>
																		</div>						    
																	<?php } ?>
																<?php endwhile; ?>
																<!-- end of the loop -->

																<!-- pagination here -->

																<?php wp_reset_postdata(); ?>

															<?php else : ?>
																<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
															<?php endif; ?>
															</div>
														  <!-- Add Arrows -->
														  <div class="swiper-button-next swiper-button-white pali-next-btn"></div>
														  <div class="swiper-button-prev swiper-button-white pali-prev-btn"></div>
														</div>
														<div class="swiper-container parliament-tab-thumbs">
														  	<div class="swiper-wrapper">
												            <?php if ( $parliament_gallery_photo_the_query->have_posts() ) : ?>
																	<!-- pagination here -->
																	<!-- the loop -->
																	<?php while ( $parliament_gallery_photo_the_query->have_posts() ) : $parliament_gallery_photo_the_query->the_post(); ?>
																		<?php if ( has_post_thumbnail() ) { ?>
																			<div class="swiper-slide slide-right-border">
																				<img src="<?php echo the_post_thumbnail_url( 'home-slider' ); ?>" class="img-responsive">
																			</div>						    
																		<?php } ?>
																	<?php endwhile; ?>
																	<!-- end of the loop -->
																	<!-- pagination here -->
																	<?php wp_reset_postdata(); ?>
																<?php else : ?>
																	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
																<?php endif; ?>
												        </div>
														</div>
										   		</div>
										    	</div>
												<!-- Parliment Photo gallery Dipolomat Tab  -->
												<div role="tabpanel" class="tab-pane" id="parliament_photo_gallery_diplomat">
													<?php 
														$diplomat_gallery_arg = array(
															'post_type' => 'post',
															'tax_query' => array(
																array(
																	'taxonomy' => 'gallery_type',
																	'field'    => 'slug',
																	'terms'    => 'diplomat',
																),
															),
														);
														// the query
														$diplomat_gallery_photo_the_query = new WP_Query( $diplomat_gallery_arg ); 
													?>
										   		<div class="parliament-section-diplomat-gallery-wrapper">
										   			<!-- Swiper -->
														<div class="swiper-container dipolomat-tab-gallery">
															<div class="swiper-wrapper">
																<?php if ( $diplomat_gallery_photo_the_query->have_posts() ) : ?>
																	<!-- pagination here -->
																	<!-- the loop -->
																	<?php while ( $diplomat_gallery_photo_the_query->have_posts() ) : $diplomat_gallery_photo_the_query->the_post(); ?>
																		<?php if ( has_post_thumbnail() ) { ?>
																			<div class="swiper-slide">
																				<div class="col-md-5 slide-left-border no-padding text-center">
																					<img src="<?php echo the_post_thumbnail_url( 'gallery-slider' ); ?>" class="img-responsive">
																				</div>
																				<div class="col-md-6">
																					<div class="slider-post-data">
																					<?php 
																						$categories = get_the_category();
																						if ( ! empty( $categories ) ) {
																							$category_name = '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
																						}
																						$post_date_ = get_the_date('M j, Y', get_the_ID());
																						echo '<div class="post-meta">'.$post_date_ .' '. $category_name.'</div>';
																						$gallery_title = get_post_meta( get_the_ID(), 'gallery_title', true );
																						echo '<div class="slider-diplomat-post-title"><h2>'.$gallery_title.'</h2></div>';
																					?>																
																					</div>
																				</div>
																			</div>						    
																		<?php } ?>
																	<?php endwhile; ?>
																	<!-- end of the loop -->

																	<!-- pagination here -->

																	<?php wp_reset_postdata(); ?>

																<?php else : ?>
																	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
																<?php endif; ?>
															</div>
														  <!-- Add Arrows -->
														  <div class="swiper-button-next swiper-button-white diplo-next-btn"></div>
														  <div class="swiper-button-prev swiper-button-white diplo-prev-btn"></div>
														</div>
														<div class="swiper-container diplomat-tab-thumbs">
														  	<div class="swiper-wrapper">
												            <?php if ( $diplomat_gallery_photo_the_query->have_posts() ) : ?>
																	<!-- pagination here -->
																	<!-- the loop -->
																	<?php while ( $diplomat_gallery_photo_the_query->have_posts() ) : $diplomat_gallery_photo_the_query->the_post(); ?>
																		<?php if ( has_post_thumbnail() ) { ?>
																			<div class="swiper-slide slide-right-border">
																				<img src="<?php echo the_post_thumbnail_url( 'home-slider' ); ?>" class="img-responsive">
																			</div>						    
																		<?php } ?>
																	<?php endwhile; ?>
																	<!-- end of the loop -->
																	<!-- pagination here -->
																	<?php wp_reset_postdata(); ?>
																<?php else : ?>
																	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
																<?php endif; ?>
												        </div>
														</div>
										   		</div>
										   	</div>
												<!-- Parliment Photo gallery Dipolomat Tab End -->
										  	</div>
							        	
							      	</div>
							    </div>
						  	</div>
						</div>
					<!-- Modal End -->				
				</div>
			</div>
		</div>
	</div><!-- Model Container -->
</div>