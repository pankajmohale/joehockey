<?php
/**
 * Template part for displaying Speeches section
 */
global $redux_demo;
?>
<div class="speeches-section-wrapper">
	<div class="container">
		<div class="row">

			<?php 
				$speech_slider_args = array(
					'post_type' => 'post',
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field'    => 'slug',
							'terms'    => 'speeches',
						),
					),
					'posts_per_page' => 5,
				);
				// the query
				$speech_slider_the_query = new WP_Query( $speech_slider_args ); 
				$count = $speech_slider_the_query->post_count;
			?>

			<?php if ( $speech_slider_the_query->have_posts() ) : ?>

				<!-- pagination here -->

				<div class="swiper-container speeches-slider-container">
			        <div class="swiper-wrapper speeches-slider-wrapper">
			        	<!-- the loop -->
					<?php while ( $speech_slider_the_query->have_posts() ) : $speech_slider_the_query->the_post(); ?>
			            <div class="swiper-slide speeches-content-wrapper">
							<div class="col-xs-12 col-sm-6">
							<div class="speech-content-wrapper">
								<?php
									$category_id = get_cat_ID( 'speeches' );
								    // Get the URL of this category
								    $category_link = get_category_link( $category_id );

									echo "<h4><a href='".esc_url( $category_link ) . "' title='Recent Speeches'>Recent Speeches</a></h4>"; 
									echo '<div class="slider-speeches-post-title color-black"><h2>'.get_the_title().'</h2></div>';
									echo '<p>'.the_excerpt_max_charlength(100).'</p>';
								?>								
							</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="speeche-video">
									<?php 
										$video_url = get_post_meta( get_the_ID(), 'video_url', true );
										echo do_shortcode('[video src="'.$video_url.'" width="700" height="465"]');									
									?>
								</div>
								<span class="speeche-count">
									<?php 
										$count_posts = $speech_slider_the_query->current_post + 1; //counts posts in loop
										echo '<span>'.$count_posts.'/'.$count.'</span>';
									?>									
								</span>
							</div>		            	
			            </div>
					<?php endwhile; ?>
					<!-- end of the loop -->
			        </div>
			        <!-- Add Pagination -->
			        <!-- <div class="swiper-pagination speeches-pagination"></div> -->
			        <!-- Add Arrows -->
			        <div class="swiper-button-next speeches-next swiper-button-white"></div>
			        <div class="swiper-button-prev speeches-prev swiper-button-white"></div>
			    </div>	

				<!-- pagination here -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

		</div>
		<div class="row">
		</div>
	</div>
</div>